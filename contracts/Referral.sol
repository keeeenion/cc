pragma solidity ^0.4.18;


import '../node_modules/zeppelin-solidity/contracts/math/SafeMath.sol';
import '../node_modules/zeppelin-solidity/contracts/lifecycle/Destructible.sol';
import '../node_modules/zeppelin-solidity/contracts/lifecycle/Pausable.sol';

import './Token.sol';
import './Crowdsale.sol';

contract Referral is Destructible, Pausable {
    using SafeMath for uint256;

    // crowdsale contract
    Crowdsale public crowdsale;
    Token public token;
    address public beneficiary;

    function Referral(Crowdsale _crowdsale, address _token, address _beneficiary) public {
        setCrowdsale(_crowdsale);
        setToken(_token);
        setBeneficiary(_beneficiary);
    }

    function setCrowdsale(address _crowdsale) onlyOwner public {
        require(_crowdsale != address(0));
        crowdsale = Crowdsale(_crowdsale);
    }

    function setToken(address _token) onlyOwner public {
        require(_token != address(0));
        token = Token(_token);
    }

    function setBeneficiary(address _beneficiary) onlyOwner public {
        require(_beneficiary != address(0));
        beneficiary = _beneficiary;
    }

    function () external whenNotPaused payable {

        uint256 tokens;
        uint256 percent;

        // buy tokens from crowdsale
        (tokens, percent) = crowdsale.buyTokens.value(msg.value)();

        token.transfer(msg.sender, tokens);

        uint256 baseAmount = crowdsale.getBaseAmount(msg.value);
        uint256 refTokens = baseAmount.div(10);

        // send 10% to referral
        token.transfer(beneficiary, refTokens);
    }
}