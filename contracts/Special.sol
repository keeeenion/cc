pragma solidity ^0.4.18;


import '../node_modules/zeppelin-solidity/contracts/math/SafeMath.sol';
import '../node_modules/zeppelin-solidity/contracts/lifecycle/Destructible.sol';
import '../node_modules/zeppelin-solidity/contracts/lifecycle/Pausable.sol';

import './Token.sol';
import './Crowdsale.sol';

contract Special is Destructible, Pausable {
    using SafeMath for uint256;

    // crowdsale contract
    Crowdsale public crowdsale;
    Token public token;

    function Special(Crowdsale _crowdsale, address _token) public {
        setCrowdsale(_crowdsale);
        setToken(_token);
    }

    function setCrowdsale(address _crowdsale) onlyOwner public {
        require(_crowdsale != address(0));
        crowdsale = Crowdsale(_crowdsale);
    }

    function setToken(address _token) onlyOwner public {
        require(_token != address(0));
        token = Token(_token);
    }

    function () external whenNotPaused payable {
        // buy tokens from crowdsale
        uint256 tokens = crowdsale.buyTokens.value(msg.value)();

        token.transfer(msg.sender, tokens);
    }
}