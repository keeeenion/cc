pragma solidity ^0.4.18;


import '../node_modules/zeppelin-solidity/contracts/ownership/Ownable.sol';
import '../node_modules/zeppelin-solidity/contracts/lifecycle/Pausable.sol';
import '../node_modules/zeppelin-solidity/contracts/lifecycle/Destructible.sol';
import '../node_modules/zeppelin-solidity/contracts/math/SafeMath.sol';

import './Token.sol';

/**
 * @title Crowdsale
 * @dev Crowdsale is a base contract for managing a token crowdsale.
 * Crowdsales have a start and end timestamps, where investors can make
 * token purchases and the crowdsale will assign them tokens based
 * on a token per ETH rate and current bonuses. Funds collected are forwarded
 * to a wallet as they arrive. The contract requires tokens.
 */
contract Crowdsale is Ownable, Pausable, Destructible {
    using SafeMath for uint256;

    struct Vault {
        uint256 tokenAmount;
        uint256 weiValue;
    }

    struct CustomContract {
        bool isReferral;
        bool isSpecial;
    }

    // The token being sold
    Token public token;

    // start and end timestamps where investments are allowed (both inclusive)
    uint256 public startTime;
    uint256 public endTime;

    // regular bonus amounts
    uint256 week_1 = 20;
    uint256 week_2 = 15;
    uint256 week_3 = 10;
    uint256 week_4 = 0;

    // custom bonus amounts
    uint256 week_special_1 = 40;
    uint256 week_special_2 = 15;
    uint256 week_special_3 = 10;
    uint256 week_special_4 = 0;

    uint256 week_referral_1 = 25;
    uint256 week_referral_2 = 20;
    uint256 week_referral_3 = 15;
    uint256 week_referral_4 = 5;

    // bonus ducks
    mapping (address => CustomContract) public customBonuses;

    // address where funds are collected
    address public wallet;

    // how many token units a buyer gets per wei
    uint256 public rate;

    // amount of raised in wei
    uint256 public weiRaised;

    // high-ballers
    mapping(address => Vault) ballers;

    event TokenPurchase(address indexed purchaser, address indexed beneficiary, uint256 value, uint256 amount);

    function Crowdsale(uint256 _startTime, uint256 _endTime, uint256 _rate, address _wallet, address _token) payable public {
        require(_startTime >= now);
        require(_endTime >= _startTime);
        require(_rate > 0);
        require(_wallet != address(0));
        require(_token != address(0));

        startTime = _startTime;
        endTime = _endTime;
        rate = _rate;
        wallet = _wallet;
        token = Token(_token);
    }

    // fallback function can be used to buy tokens
    function () external payable {
        buyTokens();
    }

    // low level token purchase function
    function buyTokens() public payable returns (uint256) {
        address beneficiary = msg.sender;

        require(beneficiary != address(0));
        require(validPurchase());

        uint256 weiAmount = msg.value;

        // calculate token amount to be sent
        uint256 tokens = getTokenAmount(weiAmount);

        // if we run out of tokens
        if (!hasEnoughTokensLeft(weiAmount)) {
            // todo: logic
        }

        // update state
        weiRaised = weiRaised.add(weiAmount);

        token.transfer(beneficiary, tokens);

        TokenPurchase(msg.sender, beneficiary, weiAmount, tokens);

        if ((11 ether) <= msg.value) {
            // we have a baller
            // add tokens to his/her vault

            ballers[beneficiary].tokenAmount += tokens;
            ballers[beneficiary].weiValue += msg.value;
            return 0;

            // todo: referral?
        }

        forwardFunds();

        return tokens;
    }

    /**
     * Release / Refund logics
     */

    function viewFunds(address _wallet) public view returns (uint256) {
        return ballers[_wallet].tokenAmount;
    }

    function releaseFunds(address _wallet) onlyOwner public {
        require(ballers[_wallet].tokenAmount > 0);
        require(ballers[_wallet].weiValue > this.balance);


        // todo: assert?
        token.transfer(_wallet, ballers[_wallet].tokenAmount);

        ballers[_wallet].tokenAmount = 0;
        ballers[_wallet].weiValue = 0;
    }

    function refundFunds(address _wallet) onlyOwner public {
        require(ballers[_wallet].tokenAmount > 0);
        require(ballers[_wallet].weiValue > this.balance);


        // todo: assert?
        _wallet.transfer(ballers[_wallet].weiValue);
        weiRaised -= ballers[_wallet].tokenAmount;

        ballers[_wallet].tokenAmount = 0;
        ballers[_wallet].weiValue = 0;
    }

    /**
     * Editors
     */

    function setCustomBonus(address _contract, bool _isReferral, bool _isSpecial) onlyOwner public {
        require(_contract != address(0));

        customBonuses[_contract] = CustomContract({
            isReferral: _isReferral,
            isSpecial: _isSpecial
        });
    }

    function changeEndTime(uint256 _endTime) onlyOwner public {
        endTime = _endTime;
    }

    function setWallet(address _wallet) onlyOwner public {
        require(_wallet != address(0));
        wallet = _wallet;
    }

    /**
     * Calculations
     */

    // @return true if crowdsale event has ended
    function hasEnded() public constant returns (bool) {
        return now > endTime;
    }

    function getBaseAmount(uint256 _weiAmount) public view returns (uint256) {
        return _weiAmount.mul(rate);
    }

    // Override this method to have a way to add business logic to your crowdsale when buying
    function getTokenAmount(uint256 _weiAmount) internal view returns (uint256) {
        uint256 tokens = getBaseAmount(_weiAmount);
        uint256 percentage = 0;

         // Special bonuses
        if (customBonuses[msg.sender].isSpecial == true) {

          if ( startTime < now && now < startTime + 7 days ) {
            percentage = week_special_1;
          } else if ( startTime + 7 days < now && now <= startTime + 14 days ) {
            percentage = week_special_2;
          } else if ( startTime + 14 days < now && now <= startTime + 21 days ) {
            percentage = week_special_3;
          } else if ( startTime + 21 days < now && now <= startTime + 28 days ) {
            percentage = week_special_4;
          }

        // Regular bonuses
        } else {

            if ( startTime < now && now <= startTime + 7 days ) {
              percentage = week_1;
            } else if ( startTime + 7 days < now && now <= startTime + 14 days ) {
              percentage = week_2;
            } else if ( startTime + 14 days < now && now <= startTime + 21 days ) {
              percentage = week_3;
            } else if ( startTime + 21 days < now && now <= endTime ) {
              percentage = week_4;
            }

        }

        // Referral bonuses
        if (customBonuses[msg.sender].isReferral == true) {
          percentage += 15; // 5 for buyer, 10 for referrer
        }

        if (msg.value >= 50 ether) {
          percentage += 80;
        } else if (msg.value >= 30 ether) {
          percentage += 70;
        } else if (msg.value >= 10 ether) {
          percentage += 50;
        } else if (msg.value >= 5 ether) {
          percentage += 30;
        } else if (msg.value >= 3 ether) {
          percentage += 10;
        }

        // is this legit?
        tokens += tokens.mul(percentage * 10).div(1000);

        assert(tokens > 0);

        return tokens;
    }

    // send ether to the fund collection wallet
    function forwardFunds() internal {
        wallet.transfer(msg.value);
    }

    // @return true if the transaction can buy tokens
    function validPurchase() internal view returns (bool) {
        bool withinPeriod = now >= startTime && now <= endTime;
        bool nonZeroPurchase = msg.value != 0;
        return withinPeriod && nonZeroPurchase;
    }

    function tokensLeft() public view returns (uint256) {
        return token.balanceOf(this) - getTokenAmount(weiRaised);
    }

    function hasEnoughTokensLeft(uint256 _weiAmount) public payable returns (bool) {
        return token.balanceOf(this) >= getBaseAmount(_weiAmount);
    }
}