pragma solidity ^0.4.18;


import '../node_modules/zeppelin-solidity/contracts/lifecycle/Destructible.sol';
import '../node_modules/zeppelin-solidity/contracts/token/ERC20/DetailedERC20.sol';
import '../node_modules/zeppelin-solidity/contracts/token/ERC20/StandardToken.sol';

contract Token is StandardToken, DetailedERC20, Destructible {
    // constructor
    function Token(string _name, string _symbol, uint8 _decimals, uint256 _totalSupply)
        DetailedERC20(_name, _symbol, _decimals) public
        {

        // covert to ether
        _totalSupply = _totalSupply;

        totalSupply_ = _totalSupply;

        // give moneyz to us
        balances[msg.sender] = totalSupply_;

        // first event
        Transfer(0x0, msg.sender, totalSupply_);
    }
}
