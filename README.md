# Solidity Project Boilerplate

A sample module using Truffle 3.0.

# Usage (via truffle)

```bash
# install project deps
npm i;
# start the testrpc server
npm run test-server;
# compile the contracts
npm run truffle -- compile;
npm run truffle -- test --network test;
# for console,
npm run truffle -- migrate --network test;
npm run truffle -- console --network test;
# recompile and run tests on file changes
npm run develop;
# publish to testnet
npm run truffle -- migrate --network ropsten # optional --reset for a full re-deploy
# do not commit `./build/contracts` unless they've got new bytecode (or re-deployed on testnet)
# and before commiting
# also, see http://truffleframework.com/docs/getting_started/packages-npm#before-publishing
npm run truffle -- networks --clean;
```

# Deploying contracts

Latest code is in wholething.sol

Live constructors:
```
Token: "CrowdCoinage", "CCOS", 18, "250000000000000000000000000"

Crowdsale(1518426000, 1520891999, 10000, <WALLET_TO_RECIEVE_FUNDS>,  <TOKEN_ADDRESS>)

Proxy(<CROWDSALE_ADDRESS>, <TOKEN_ADDRESS>)
```

Testing constructors:
```
Token: "CrowdCoinage", "CCOS", 18, "250000000000000000000000000"

Crowdsale(1514797200, 1520845200, 10000, <WALLET_TO_RECIEVE_FUNDS>,  <TOKEN_ADDRESS>)

Proxy(<CROWDSALE_ADDRESS>, <TOKEN_ADDRESS>)
```


After deploying
1. Send tokens to Crowdsale
2. Register Proxies if any (setCustomBonus) (If not referral, set referralAddress to 0x0)

KYC
For funds bigger than 11 ETH, we have to conduct a KYC. Funds get stashed into 'ballers' mapping. To release any funds call the function 'releaseFunds(<INVESTOR_ADDRESS>)', to refund all stashed funds call 'refundFunds(<INVESTOR_ADDRESS>)'

# Possible issues
1. Bitcore [Hackish fix](https://github.com/bitpay/bitcore/issues/1454)

For more information on truffle CLI, see http://truffleframework.com/docs/advanced/commands.

---

Join our online chat at [![Gitter](https://badges.gitter.im/gitterHQ/gitter.svg)](https://gitter.im/DigixGlobal/devtools)
