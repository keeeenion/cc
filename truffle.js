const HDWalletProvider = require("truffle-hdwallet-provider");

const infura_apikey = "6UxkpyBQ5ZBKCkory5SA";
const mnemonic = "explain cereal artist hawk example melt priority bacon pig torch resource glue";

module.exports = {
  networks: {
    // ropsten: {
    //   network_id: 3,
    //   host: "localhost",
    //   port:  8545,
    //   gas:   2900000
    // },
    ropsten: {
      provider: new HDWalletProvider(mnemonic, "https://ropsten.infura.io/"+infura_apikey),
      network_id: 3,
      gas: 4700000,
      // gasPrice: 10000000000
    },
    test: {
      host: "localhost",
      port: 6545,
      network_id: '*',
      gas: 4712388,
      gasPrice: 10000000000
    }
  },
};
