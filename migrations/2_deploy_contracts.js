// imports
// const contract = require('../node_modules/truffle-contract');
// const data = require('../node_modules/@digix/solidity-core-libraries/build/contracts/MathUtils.json');
// const MathUtils = contract(data);

// local contracts
const TokenContract = artifacts.require('./Token.sol');
const CrowdsaleContract = artifacts.require('./Crowdsale.sol');

const startUnixtime = (new Date()).getTime();
const endUnixtime = (new Date('2018-03-12T11:00:00Z')).getTime();
const ourWallet = 0;

module.exports = function (deployer) {
  deployer.deploy(TokenContract, 'CrowdCoinage', 'CCOS', 18, '250000000000000000000000000')
    .then(function (i) {
      return deployer.deploy(CrowdsaleContract, startUnixtime, endUnixtime, 10000, ourWallet, TokenContract.address);
      // todo: send moneys to crowdsale contract
    });

    // Real commands:
    // "CrowdCoinage", "CCOS", 18, "250000000000000000000000000"
    // 1518426000, 1520845200, 10000, "",  ""

    // For testing:
    // "CrowdCoinage", "CCOS", 18, "250000000000000000000000000"
    // 1514797200, 1520845200, 10000, "",  ""

    // After deploying
    // Send tokens to Crowdsale contract
    // Register Proxies if any (setCustomBonus)
    // if not referral, set referralAddress to 0x0
};
